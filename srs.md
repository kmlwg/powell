# Metoda Powella
## Opis
Metoda Powella (kierunków sprzężonych) jest bezgradientową, iteracyjną metodą optymalizacji bez ograniczeń. Dla form kwadratowych $`F:R^N\rightarrow R`$ jest zbieżna w $`N`$ krokach. Ideą metody jest badanie zachowania funkcji wykonując kroki w kierunkach pewnej bazy. W trakcie wykonywania algorytmu, modyfikowna jest baza - dodawany jest do niej nowy sprzężony kierunek i równocześnie usuwany jest z niej kierunek, wzdłuż którego natąpiło największe przesunięcie.
## Zarys
1. Niech $`P_0=X_i`$
2. Dla $`k=1,2,...,N`$ znajdź $`\gamma_k`$ minmalizujące $`f(P_{k-1}-\gamma_kU_k)`$ i ustal $`P_k=P_{k-1}+\gamma_kU_k`$
3. $`r`$ i $`U_r`$ równe są największemu spakowi w $`f`$ i kierunkowi największego spadku dla wszystkich wektorów kierunkowych w 2.
4. $`i=i+1`$
5. Jeśli $`f(2P_n-P_0)\geq f(P_0)`$ albo $`2(f(P_0)-2f(P_N)+f(2P_N-P_0))(f(P_0)-f(P_N)-r)^2\geq r(f(P_0)-f(2P_N-P_0)`$, to $`X_i=P_N`$ i wróć do kroku 1. W innym wypadku przejdź do kroku 6.
6. $`U_r=P_N-P_0`$
7. Znajdź wartość $`\gamma`$ która minimalizuje $`f(P_0+\gamma U_r)`$. $`X_i = P_0+\gamma U_r`$.
8. Powtórz kroki od 1. do 7.
# Terminarz

|   LP | Termin wykonania | Zadanie                                      |
| ---: | :--------------- | :------------------------------------------- |
|    1 | 17.05.2019       | Przygotowanie specyfikacji                   |
|    2 | 17.05.2019       | Przygotowanie repozytorium, dodanie licencji |
|    3 | 24.05.2019       | Wykonanie połowy projektu                    |
|    4 | 14.06.2019       | Ukończenie projektu                          |
|    5 | 14.06.2019       | Dokumentacja projektu                        |

## Zadania do wykonania
### 1. Wykonanie połowy projektu:
* implementacja algorytmu w wersji protypowej
* testy poprawności wykonania
### 2. Ukończenie projektu:
* refaktoryzacja kodu
* zintegrowanie z pakietem Optim.jl
* test poprawności wykonania
* sporządzenie dokumentacji
# Literatura / Źródła
1. "An efficient method for finding the minimum of a function of several variables without calculating derivatives" - M.J.D Powell (The Computer Journal, Volume 7, Issue 2, 1964, Pages 155–162)
2. "Numerical Methods Using MatLab IV Edition" (2004) - John H. Mathews, Kurtis D. Fink (Pages 434-439)
3. Przykład implementacji w bibliotece SciPy - https://github.com/scipy/scipy/blob/master/scipy/optimize/optimize.py
