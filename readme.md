# Powell's method
## Description
Powell's method is gradient free, iterative method of non constrained optimalization. For quadratic forms $`F:R^N\rightarrow R`$ is convergent in 
$`N`$  steps. General idea of the method is to analyze a function while making move towards some base. During execution of the algorithm the base is modified - new conjugate direction is added while the previous one is deleted.
### Outline

1. Let $`P_0=X_i`$
2. For $`k=1,2,...,N`$ find $`\gamma_k`$ minimizing $`f(P_{k-1}-\gamma_kU_k)`$ and set $`P_k=P_{k-1}+\gamma_kU_k`$
3. $`r`$ i $`U_r`$ are equal to biggest descent in $`f`$ and direction of the biggest descent for all directional vectors in 2.
4. $`i=i+1`$
5. If $`f(2P_n-P_0)\geq f(P_0)`$ or $`2(f(P_0)-2f(P_N)+f(2P_N-P_0))(f(P_0)-f(P_N)-r)^2\geq r(f(P_0)-f(2P_N-P_0)`$, to $`X_i=P_N`$ come back to step 1. Otherwise step 6.
6. $`U_r=P_N-P_0`$
7. Find value $`\gamma`$ minimizing $`f(P_0+\gamma U_r)`$. $`X_i = P_0+\gamma U_r`$.
8. Repeat 1. through 7.

The implementation is based on "Numerical Methods Using MatLab IV Edition" (2004) - John H. Mathews, Kurtis D. Fink and SciPy implementation.  

## Usage and details
```julia
powell(f, X0, (maxIter))
```
#### Arguments
``` julia
Function:: f(X)
    Objective function to be minimized
Array:: X0
    Initial guess
Int:: maxIter, optional
    Maximum number of iterations to perform, default is 1000
```
For finding next point of interest in the first stage of the algorithm, 
Brent's method from Optim.jl is used  
  
#### The results are returned as dictionary
```julia
Dict{String, Any} (
    "name" => String
        name of the algorithm
    "start_point" => AbstractArray{Float64}
        starting point for algorithm
    "minimizer" => AbstractArray{Float64}
        values minimizing function
    "minimum" => Float
        found minimal value of function
    "num_iter" => Integer
        number of iterations used fo calculation
    "reached_max" => Bool
        wether maximum number of iterations was reache
    )
```
#### Example
``` julia
julia> function booth(X::AbstractArray)
          return (X[1] + 2*X[2] - 7)^2 + (2*X[1] + X[2] - 5)^2
       end

julia> res = powell(booth, [2.,4.],100)
julia> print_results(res)
Results of Optimization Algorithm
 * Algorithm: Powell's method
 * Starting point: [2.0, 4.0]
 * Minimizer: [1.0, 3.0]
 * Minimum: 0.0
 * Number of iterations: 78
 * Reached max number of iterations: false

```

## References
1. "An efficient method for finding the minimum of a function of several variables without calculating derivatives" - M.J.D Powell (The Computer Journal, Volume 7, Issue 2, 1964, Pages 155–162)
2. "Numerical Methods Using MatLab IV Edition" (2004) - John H. Mathews, Kurtis D. Fink (Pages 434-439)
3. https://github.com/scipy/scipy/blob/master/scipy/optimize/optimize.py
