using Optim
using LinearAlgebra
# optimize(f, lower, upper, Brent(); kwargs...)
function linsearch_powell(f::Function, p, xi, tol=1e-3)
    function myfunc(gamma)
        return(f(p + gamma*xi))
    end
    res = optimize(myfunc, -10., 10. , Brent())
    xi = res.minimizer * xi
    return res.minimum, p  + xi, xi
end

function powell(f::Function, X0::AbstractArray{Float64}, maxIter=1000::Int)
    """
    Minimize function using Powell's method
    References:
        [1] "Numerical Methods Using MatLab IV Edition" (2004)
              - John H. Mathews, Kurtis D. Fink (Pages 434-439)
        [2] https://github.com/scipy/scipy/blob/master/scipy/optimize/optimize.py

    Parameters
    ==========
    Function:: f(X)
        Objective function to be minimized
    Array:: X0
        Initial guess
    maxIter:: Int, optional
        Maximum number of iterations to perform

    Returns
    =======
    Dict{String, Any} (
        "name" => String
            name of the algorithm
        "start_point" => AbstractArray{Float64}
            starting point for algorithm
        "minimizer" => AbstractArray{Float64}
            values minimizing function
        "minimum" => Float
            found minimal value of function
        "num_iter" => Integer
            number of iterations used fo calculation
        "reached_max" => Bool
            wether maximum number of iterations was reache
    )

    """
    iter = 1
    dir = 0
    U = Array{Float64}(I, length(X0), length(X0))  # base vectors
    P = X0
    P_old = P
    P1 = X0
    fval = f(X0)
    bigind = 0
    delta = 0.
    reached_max = false
    while(true)
        fx = fval
        for i in 1:length((X0))
            dir = U[i , 1:2]
            fx2 = fval
            fval, P, dir = linsearch_powell(f, P, dir)

            if (fx2 - fval) > delta
                delta = fx2 - fval
                bigind = i
            end
        end
        if P_old == P
            reached_max = false
            break
        end
        iter += 1
        if iter >= maxIter
            reached_max = true
            break
        end
        dir1 = P - P1
        P2 = 2*P - P1
        P_old = P
        fx2 = f(P2)
    end

    return Dict("name" => "Powell's method", "start_point" => X0, "minimizer" => P,
        "minimum" => fval, "num_iter" => iter, "reached_max" => reached_max)
end

function print_resutls(res::Dict)
    println("Results of Optimization Algorithm")
    print(" * Algorithm: ")
    println(res["name"])
    print(" * Starting point: ")
    println(res["start_point"])
    print(" * Minimizer: ")
    println(res["minimizer"])
    print(" * Minimum: ")
    println(res["minimum"])
    print(" * Number of iterations: ")
    println(res["num_iter"])
    print(" * Reached max number of iterations: ")
    println(res["reached_max"])
end

function booth(X::AbstractArray)
    return (X[1] + 2*X[2] - 7)^2 + (2*X[1] + X[2] - 5)^2
end


res = powell(booth, [2.,4.],100)
print_resutls(res)
